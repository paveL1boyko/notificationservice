from pathlib import Path

from dotenv import load_dotenv

load_dotenv(dotenv_path=Path(".env.local"))

from app.frameworks_and_drivers.wsgi import app  # noqa E402


if __name__ == "__main__":
    app.run(debug=True)
