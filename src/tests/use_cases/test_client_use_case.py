import pytest

from app.adapters.sql_alcemy.client_repository import SQLAlchemyClientRepository
from app.use_cases.client_use_case import ClientUseCase
from tests.api.client.test_client_factory import (
    ClientEntityFactory,
    ClientDBFactory,
    TagFactory,
    START_PHONE_NUMBER,
)
from tests.conftest import MainTestConfig


class TestClientRepository(MainTestConfig):
    @pytest.fixture(scope="class")
    def client_use_case(self):
        return ClientUseCase(SQLAlchemyClientRepository())

    def test_add_client(self, client_use_case):
        data = ClientEntityFactory.build()
        assert data.dict() == client_use_case.create(data).dict(exclude={"id"})

    def test_delete_client(self, client_use_case):
        data = ClientDBFactory.create()
        assert client_use_case.delete(data.id) is True
        assert client_use_case.delete(data.id) is False

    def test_update_client(self, client_use_case):
        data = ClientDBFactory.create()
        updated_data = ClientEntityFactory.build()
        client_use_case.update(client_id=data.id, client_data=updated_data)

        db_client = client_use_case.get_client_by_id(data.id)
        assert updated_data.dict() == db_client.dict(exclude={"id"})

    @pytest.fixture(autouse=True)
    def reset_factory_sequences(self):
        ClientDBFactory.reset_sequence(0)

    @pytest.mark.parametrize(
        "tags, phones, expected_count",
        [
            ([], [START_PHONE_NUMBER], 1),  # Test with only phone number
            (["tag2"], [], 2),  # Test with only tags
            (["tag4"], [START_PHONE_NUMBER + 1], 2),  # Test with both tags and phone number
            (["tag4"], [START_PHONE_NUMBER + 2], 1),  # Test with tag and specific phone number
        ],
    )
    def test_get_clients_by_tags_or_phone(self, tags, phones, expected_count, client_use_case):
        ClientEntityFactory.reset_sequence(0, force=True)
        tag1 = TagFactory.create(name="tag1")
        tag2 = TagFactory.create(name="tag2")
        tag3 = TagFactory.create(name="tag3")
        tag4 = TagFactory.create(name="tag4")

        client1 = ClientEntityFactory.build(tags=[tag1.name, tag2.name])
        client2 = ClientEntityFactory.build(tags=[tag2.name, tag3.name])
        client3 = ClientEntityFactory.build(tags=[tag4.name])

        client_use_case.create(client1)
        client_use_case.create(client2)
        client_use_case.create(client3)

        clients = client_use_case.get_clients_by_tags_or_phone(tags=tags, phones=phones)
        assert len(clients) == expected_count
