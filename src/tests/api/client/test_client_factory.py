import random

import factory.alchemy

from app.entities.client.client_entity import ClientEntityRequest
from app.frameworks_and_drivers.orm_models.models import Client, Tag
from tests.test_base_factory import BaseFactory

START_PHONE_NUMBER = 70000000000


class TagFactory(BaseFactory):
    name = factory.Faker("word")

    class Meta:
        model = Tag


class ClientDBFactory(BaseFactory):
    id = factory.Sequence(lambda n: n)
    phone_number = factory.Sequence(lambda n: START_PHONE_NUMBER + n)
    operator_code = factory.Faker("random_int", min=0, max=999)
    timezone = factory.Faker("timezone")
    tags = factory.List([factory.SubFactory(TagFactory) for _ in range(random.randint(1, 5))])

    class Meta:
        model = Client


class ClientEntityFactory(ClientDBFactory):
    tags = factory.List([factory.Faker("word") for _ in range(random.randint(0, 5))])

    class Meta:
        model = ClientEntityRequest
