from http import HTTPStatus

import pytest

from app.adapters.sql_alcemy.client_repository import SQLAlchemyClientRepository
from app.entities.client.client_entity import (
    ClientEntityRequest,
    ClientEntityResponse,
)
from app.frameworks_and_drivers.orm_models.models import Client
from tests.api.client.test_client_factory import (
    ClientDBFactory,
    ClientEntityFactory,
)
from tests.conftest import MainTestConfig


class TestClientView(MainTestConfig):
    client_path = MainTestConfig.api_path + "/client/"

    @pytest.fixture(scope="class")
    def client_repository(self):
        """
        Pytest fixture that initializes and returns an instance of the SQLAlchemyClientRepository.
        This repository provides methods to interact with the database
         for client-related operations.
        """
        return SQLAlchemyClientRepository()

    def test_post_client(self, api_client, client_repository):
        data: ClientEntityRequest = ClientEntityFactory.build()
        response = api_client.post(self.client_path, json=data.dict())
        response_data = ClientEntityResponse(**response.json)

        # Check if the response status code is CREATED
        assert response.status_code == HTTPStatus.CREATED
        # Check if the returned data matches the data in the database
        assert response_data.dict() == client_repository.get_client_by_id(response_data.id).dict()

    def test_update_client(self, api_client, client_repository):
        test_client: Client = ClientDBFactory.create()
        data: ClientEntityRequest = ClientEntityFactory.build()
        response = api_client.put(f"{self.client_path}{test_client.id}", json=data.dict())
        response_data = ClientEntityResponse(**response.json)

        # Check if the response status code is OK
        assert response.status_code == HTTPStatus.OK
        # Check if the returned data matches the updated data in the database
        assert response_data.dict() == client_repository.get_client_by_id(response_data.id).dict()

    def test_delete_client(self, api_client, client_repository):
        test_client = ClientDBFactory.create()

        # Ensure the client exists before deletion
        assert client_repository.get_client_by_id(test_client.id)

        response = api_client.delete(f"{self.client_path}{test_client.id}")

        # Check if the response status code indicates that the content has been deleted
        assert response.status_code == HTTPStatus.NO_CONTENT
        # Ensure the client no longer exists in the database
        assert client_repository.get_client_by_id(test_client.id) is None
