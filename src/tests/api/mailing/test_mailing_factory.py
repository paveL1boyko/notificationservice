import random
from datetime import datetime, timedelta

import factory.alchemy

from app.entities.mailing.mailing_entity import MailingEntityRequest
from app.frameworks_and_drivers.orm_models.models import Mailing
from tests.test_base_factory import phone_generator, BaseFactory


def phone_numbers_generator():
    return [phone_generator() for _ in range(random.randint(2, 6))]


class MailingDBFactory(BaseFactory):
    id = factory.Sequence(lambda n: n)
    start_time = factory.LazyFunction(lambda: datetime.utcnow())
    message_text = factory.Faker("sentence")
    phone_numbers_filter = factory.LazyFunction(phone_numbers_generator)
    tags_filter = factory.List([factory.Faker("word") for _ in range(random.randint(0, 5))])
    end_time = factory.LazyFunction(lambda: datetime.utcnow() + timedelta(hours=1))

    class Meta:
        model = Mailing


class MailingEntityFactory(MailingDBFactory):
    class Meta:
        model = MailingEntityRequest
