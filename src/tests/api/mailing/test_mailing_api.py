from http import HTTPStatus
from unittest import mock

import pytest

from app.adapters.sql_alcemy.mailing_repository import SQLAlchemyMailingRepository
from app.entities.mailing.mailing_entity import (
    MailingEntityRequest,
    MailingEntityResponse,
)
from app.frameworks_and_drivers.orm_models.models import Mailing
from tests.api.mailing.test_mailing_factory import (
    MailingDBFactory,
    MailingEntityFactory,
)
from tests.conftest import MainTestConfig


class TestMailingView(MainTestConfig):
    mailing_path = MainTestConfig.api_path + "/mailing/"

    @pytest.fixture(scope="class")
    def mailing_repository(self):
        """
        Pytest fixture that initializes and returns an instance of the SQLAlchemyMailingRepository.
        This repository provides methods to interact with the database
        for mailing-related operations.
        """
        return SQLAlchemyMailingRepository()

    @mock.patch("app.adapters.celery_task.task.send_mailing_to_client.apply_async")
    def test_post_mailing(
        self,
        mock_mailing,
        api_client,
        mailing_repository,
    ):
        data: MailingEntityRequest = MailingEntityFactory.build()
        response = api_client.post(self.mailing_path, json=data.dict())
        response_data = MailingEntityResponse(**response.json)

        # Check if the response status code is CREATED
        assert response.status_code == HTTPStatus.CREATED
        # Check if the returned data matches the data in the database
        assert response_data.dict() == mailing_repository.get_mailing_by_id(response_data.id).dict()
        assert mock_mailing.called

    @mock.patch("app.adapters.celery_task.task.send_mailing_to_client.apply_async")
    def test_update_mailing(self, mock_mailing, api_client, mailing_repository):
        mailing: Mailing = MailingDBFactory.create()
        data: MailingEntityRequest = MailingEntityFactory.build()
        response = api_client.put(f"{self.mailing_path}{mailing.id}", json=data.dict())
        response_data = MailingEntityResponse(**response.json)

        # Check if the response status code is OK
        assert response.status_code == HTTPStatus.OK
        # Check if the returned data matches the updated data in the database
        assert response_data.dict() == mailing_repository.get_mailing_by_id(response_data.id).dict()
        # assert mock_mailing.called

    def test_delete_mailing(self, api_client, mailing_repository):
        test_mailing = MailingDBFactory.create()

        # Ensure the mailing exists before deletion
        assert mailing_repository.get_mailing_by_id(test_mailing.id)

        response = api_client.delete(f"{self.mailing_path}{test_mailing.id}")

        # Check if the response status code indicates that the content has been deleted
        assert response.status_code == HTTPStatus.NO_CONTENT.value
        # Ensure the mailing no longer exists in the database
        assert mailing_repository.get_mailing_by_id(test_mailing.id) is None

    @mock.patch("app.adapters.celery_task.task.send_mailing_to_client.apply_async")
    def test_delete_mailing_not_found(self, mock_mailing, api_client):
        """
        Test the scenario where we try to delete a mailing that doesn't exist.
        The API should return a not found status code.
        """
        non_existent_id = 9999
        response = api_client.delete(f"{self.mailing_path}{non_existent_id}")

        assert response.status_code == HTTPStatus.NOT_FOUND
        assert "not found" in response.json["error"]

    @mock.patch("app.adapters.celery_task.task.send_mailing_to_client.apply_async")
    def test_update_mailing_not_found(self, mock_mailing, api_client):
        """
        Test the scenario where we try to update a mailing that doesn't exist.
        The API should return a not found status code.
        """
        non_existent_id = 9999
        data: MailingEntityRequest = MailingEntityFactory.build()
        response = api_client.put(f"{self.mailing_path}{non_existent_id}", json=data.dict())

        assert response.status_code == HTTPStatus.NOT_FOUND
        assert "not found" in response.json["error"]
