import random

import factory.alchemy

from app.adapters.sql_alcemy.base_repository import BaseSQLAlchemyRepository


def phone_generator():
    return int("7" + "".join([str(random.randint(0, 9)) for _ in range(10)]))


class BaseFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        abstract = True
        sqlalchemy_session = BaseSQLAlchemyRepository.db.session
        sqlalchemy_session_persistence = "commit"
