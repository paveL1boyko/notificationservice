from pathlib import Path

import pytest
from dotenv import load_dotenv

load_dotenv(dotenv_path=Path("../.env.local"))

from app.adapters.sql_alcemy.base_repository import BaseSQLAlchemyRepository  # noqa E402
from app.frameworks_and_drivers.wsgi import app  # noqa E402
from tests.api.client.test_client_factory import ClientDBFactory  # noqa E402


@pytest.fixture
def client():
    user = ClientDBFactory.create()
    yield user
    BaseSQLAlchemyRepository.db.session.delete(user)


@pytest.fixture(scope="class")
def app_context():
    with app.app_context():
        yield


@pytest.fixture(scope="class")
def api_client(app_context):
    with app.test_client() as client:
        yield client


class MainTestConfig:
    api_path = "api"

    @pytest.fixture(scope="class")
    def base_db_repository(self):
        yield BaseSQLAlchemyRepository()

    @pytest.fixture(scope="class", autouse=True)
    def setup_class(self, app_context):
        ...

    @pytest.fixture(scope="function", autouse=True)
    def create_db(self, base_db_repository):
        base_db_repository.drop_database()
        base_db_repository.create_database()
