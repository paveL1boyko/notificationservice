import factory

from app.adapters.mailing_service.models import ResponseModel


class ResponseModelFactory(factory.Factory):
    """Factory for the ResponseModel."""

    code = factory.Faker("random_int", min=100, max=999)
    message = factory.Faker("sentence")

    class Meta:
        model = ResponseModel
