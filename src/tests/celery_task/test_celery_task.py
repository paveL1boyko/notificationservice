from unittest.mock import patch, MagicMock, AsyncMock

import aiohttp

from app.adapters.celery_task.task import send_mailing_to_client
from app.adapters.sql_alcemy.message_repository import SQLAlchemyMessageRepository
from app.entities.message.message_dto import StatusEnum
from app.use_cases.message_use_case import MessageUseCase
from tests.api.client.test_client_factory import ClientDBFactory
from tests.api.mailing.test_mailing_factory import MailingDBFactory
from tests.celery_task.test_celery_factory import ResponseModelFactory
from tests.conftest import MainTestConfig


class TestCeleryTask(MainTestConfig):
    def _setup_mock(self, mock_post, status=200):  # 400 или другой статус ошибки
        mock_response = MagicMock(status=status)
        if status != 200:
            mock_response.raise_for_status.side_effect = aiohttp.ClientError()
        response_data = ResponseModelFactory.build()
        mock_response.json = AsyncMock(return_value=response_data.dict())
        mock_post.return_value.__aenter__.return_value = mock_response

    def _create_clients_for_mailing(self, phone_numbers_filter):
        for phone in phone_numbers_filter:
            ClientDBFactory.create(phone_number=phone)

    @patch("aiohttp.ClientSession.post")
    def test_send_requests(self, mock_post):
        self._setup_mock(mock_post)

        mailing = MailingDBFactory.create()
        self._create_clients_for_mailing(mailing.phone_numbers_filter)

        message_use_case = MessageUseCase(SQLAlchemyMessageRepository())

        # call task
        send_mailing_to_client.apply(kwargs=dict(mailing_id=mailing.id))
        messages = message_use_case.get_message_by_mailing_id(mailing_id=mailing.id)

        assert len(messages) == len(mailing.phone_numbers_filter)
        assert mock_post.called

    @patch("aiohttp.ClientSession.post")
    def test_send_requests_when_service_not_answered(self, mock_post):
        self._setup_mock(mock_post, status=500)

        mailing = MailingDBFactory.create()
        self._create_clients_for_mailing(mailing.phone_numbers_filter)

        message_use_case = MessageUseCase(SQLAlchemyMessageRepository())

        # call task
        send_mailing_to_client.apply(kwargs=dict(mailing_id=mailing.id))
        messages = message_use_case.get_message_by_mailing_id(mailing_id=mailing.id)

        assert len(messages) == len(mailing.phone_numbers_filter)
        assert all(message.status == StatusEnum.failed for message in messages)
        assert mock_post.called
