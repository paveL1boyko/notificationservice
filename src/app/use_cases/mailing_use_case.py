from app.entities.mailing.mailing_dto import MailingDTO
from app.entities.mailing.mailing_entity import (
    MailingEntityResponse,
    MailingEntityRequest,
)
from app.interfaces.mailing_repository import MailingRepositoryInterface


class MailingUseCase:
    def __init__(self, repo: MailingRepositoryInterface):
        self.repo = repo

    def add(self, mailing_data: MailingEntityRequest) -> MailingEntityResponse:
        mailing = self.repo.create(MailingDTO(**mailing_data.dict()))
        return MailingEntityResponse(**mailing.dict())

    def update(self, mailing_id: int, mailing_data: MailingEntityRequest) -> MailingEntityResponse:
        mailing = self.repo.update(mailing_id, MailingDTO(**mailing_data.dict()))
        return MailingEntityResponse(**mailing.dict())

    def delete(self, mailing_id: int):
        return self.repo.delete(mailing_id=mailing_id)

    def get_mailing_by_id(self, mailing_id: int):
        mailing = self.repo.get_mailing_by_id(mailing_id=mailing_id)
        return MailingEntityResponse(**mailing.dict())
