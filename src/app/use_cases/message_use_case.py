from app.entities.message.message_dto import MessageDTO
from app.entities.message.message_entity import (
    MessageEntityResponse,
    MessageEntityRequest,
)
from app.interfaces.message_repository import MessageRepositoryInterface


class MessageUseCase:
    def __init__(self, repo: MessageRepositoryInterface):
        """
        :param repo: The repository interface for messages.
        """
        self.repo = repo

    def create(self, message_data: MessageEntityRequest) -> MessageEntityResponse:
        """
        Add a new message to the repository.

        :param message_data: Data for the new message.
        :return: The created message with its ID.
        """
        message = self.repo.create(MessageDTO(**message_data.dict()))
        return MessageEntityResponse(**message.dict())

    def update(self, message_id: int, message_data: MessageEntityRequest) -> MessageEntityResponse:
        """
        Update an existing message in the repository.

        :param message_id: ID of the message to update.
        :param message_data: New data for the message.
        :return: The updated message.
        """
        message = self.repo.update(message_id, MessageDTO(**message_data.dict()))
        return MessageEntityResponse(**message.dict())

    def delete(self, message_id: int):
        """
        Delete a message from the repository.

        :param message_id: ID of the message to delete.
        """
        return self.repo.delete(message_id=message_id)

    def get_message_by_mailing_id(self, mailing_id: int) -> list[MessageEntityResponse]:
        messages = self.repo.get_message_by_mailing_id(mailing_id=mailing_id)
        return [MessageEntityResponse(**message.dict()) for message in messages]

    def create_or_update(self, message_data: MessageEntityRequest) -> MessageEntityResponse:
        """
        Add a new message to the database or update an existing one.

        :param message_data: Message data transfer object
        :return: MessageDTO of the added or updated message
        """
        message = self.repo.create_or_update(MessageDTO(**message_data.dict()))
        return MessageEntityResponse(**message.dict())
