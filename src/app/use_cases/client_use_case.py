from app.entities.client.client_dto import ClientDTO
from app.entities.client.client_entity import ClientEntityResponse, ClientEntityRequest
from app.interfaces.client_repository import ClientRepositoryInterface


class ClientUseCase:
    def __init__(self, repo: ClientRepositoryInterface):
        self.repo = repo

    def create(self, client_data: ClientEntityRequest):
        client = self.repo.create(ClientDTO(**client_data.dict()))
        return ClientEntityResponse(**client.dict())

    def update(self, client_id: int, client_data: ClientEntityRequest) -> ClientEntityResponse:
        client = self.repo.update(client_id, ClientDTO(**client_data.dict()))
        return ClientEntityResponse(**client.dict())

    def delete(self, client_id: int):
        return self.repo.delete(client_id=client_id)

    def get_client_by_id(self, client_id: int) -> ClientEntityResponse | None:
        return self.repo.get_client_by_id(client_id=client_id)

    def get_clients_by_tags_or_phone(
        self, tags: list[str] | None = None, phones: list[int] | None = None
    ) -> list[ClientEntityResponse]:
        clients = self.repo.get_clients_by_tags_or_phone(tags=tags, phones=phones)
        return [ClientEntityResponse(**client.dict()) for client in clients]
