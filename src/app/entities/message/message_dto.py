from datetime import datetime
from enum import Enum

from pydantic.v1 import BaseModel


class StatusEnum(str, Enum):
    sent = "sent"
    failed = "failed"
    pending = "pending"


class MessageDTO(BaseModel):
    id: int | None
    creation_time: datetime
    status: StatusEnum
    mailing_id: int | None
    client_id: int | None

    class Config:
        orm_mode = True
