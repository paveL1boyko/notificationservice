from datetime import datetime

from pydantic.v1 import BaseModel, Field

from app.entities.message.message_dto import StatusEnum


class MessageEntityRequest(BaseModel):
    creation_time: datetime = Field(
        default_factory=datetime.utcnow, description="The creation time of the message."
    )
    status: StatusEnum = Field(StatusEnum.sent, description="The status of the message.")
    mailing_id: int = Field(..., description="The ID of the mailing associated with the message.")
    client_id: int = Field(..., description="The ID of the client associated with the message.")


class MessageEntityResponse(MessageEntityRequest):
    id: int = Field(..., description="Unique identifier for the message.")
