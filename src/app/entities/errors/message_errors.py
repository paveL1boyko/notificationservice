class MessageError(Exception):
    """
    Base exception for all message-related errors.
    """

    pass


class MessageNotFoundError(MessageError):
    """
    Raised when a specific message is not found in the database.
    """

    def __init__(self, message: str):
        super().__init__(f"{message} not found.")
        self.message_id = message


class MessageDatabaseError(MessageError):
    """
    Raised when a generic database error occurs related to messages.
    """

    def __init__(self, original_exception: Exception):
        super().__init__(f"A database error occurred: {str(original_exception)}")
        self.original_exception = original_exception
