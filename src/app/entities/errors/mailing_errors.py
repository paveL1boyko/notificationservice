class MailingError(Exception):
    """
    Base exception for all mailing-related errors.
    """

    def __init__(self, message: str):
        super().__init__(message)


class MailingNotFoundError(MailingError):
    """
    Raised when a specific mailing is not found in the database.
    """

    def __init__(self, mailing: int):
        super().__init__(f"{mailing} not found.")
        self.mailing_id = mailing


class MailingAlreadyExistsError(MailingError):
    """
    Raised when trying to add a mailing that already exists.
    """

    def __init__(self, mailing: str):
        super().__init__(f"{mailing} already exists.")
        self.mailing_name = mailing


class MailingDatabaseError(MailingError):
    """
    Raised when a generic database error occurs related to mailings.
    """

    def __init__(self, original_exception: Exception):
        super().__init__(f"A database error occurred: {str(original_exception)}")
        self.original_exception = original_exception
