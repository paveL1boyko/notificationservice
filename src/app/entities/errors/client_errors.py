class ClientError(Exception):
    """
    Base exception for all client-related errors.
    """

    def __init__(self, message: str):
        super().__init__(message)


class ClientNotFoundError(ClientError):
    """
    Raised when a specific client is not found in the database.
    """

    def __init__(self, client: str):
        super().__init__(f"{client} not found.")
        self.client_id = client


class ClientAlreadyExistsError(ClientError):
    """
    Raised when trying to add a client that already exists.
    """

    def __init__(self, client_name: str):
        super().__init__(f"{client_name} already exists.")
        self.client_name = client_name
