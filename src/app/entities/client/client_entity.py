from pydantic.v1 import Field, BaseModel


class ClientEntityRequest(BaseModel):
    phone_number: int = Field(
        ...,
        description="The client's phone "
        "number in the format "
        "7XXXXXXXXXX (X - digit from 0 to 9)",
    )
    operator_code: int = Field(..., description="The mobile operator code of the client")
    tags: list[str] = Field(..., description="Arbitrary labels for the client")
    timezone: str = Field(..., description="The time zone of the client")


class ClientEntityResponse(ClientEntityRequest):
    id: int = Field(..., description="Unique identifier for the client.")
