from pydantic.v1 import BaseModel, validator

from app.frameworks_and_drivers.orm_models.models import Tag


class TagDTO(BaseModel):
    name: str

    class Config:
        orm_mode = True


class ClientDTO(BaseModel):
    id: int | None
    phone_number: int
    operator_code: int
    tags: list[str]
    timezone: str

    @validator("tags", pre=True, always=True, each_item=True)
    def convert_tags(cls, tag: str | Tag):
        return tag.name if isinstance(tag, Tag) else tag

    class Config:
        orm_mode = True
