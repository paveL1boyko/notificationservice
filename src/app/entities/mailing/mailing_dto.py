from datetime import datetime

from pydantic.v1 import BaseModel


class MailingDTO(BaseModel):
    id: int | None
    start_time: datetime
    message_text: str
    phone_numbers_filter: list[int]
    tags_filter: list[str]
    end_time: datetime

    class Config:
        orm_mode = True
