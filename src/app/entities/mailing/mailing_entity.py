from datetime import datetime

from pydantic.v1 import BaseModel, Field


class MailingEntityRequest(BaseModel):
    start_time: datetime = Field(..., description="The start time of the mailing.")
    message_text: str = Field(..., description="The text of the message for the mailing.")
    phone_numbers_filter: list[int] = Field(
        [],
        description="List of phone numbers to filter" " the clients for the mailing.",
    )
    tags_filter: list[str] = Field(
        [], description="List of tags to filter" " the clients for the mailing."
    )
    end_time: datetime = Field(..., description="The end time of the mailing.")


class MailingEntityResponse(MailingEntityRequest):
    id: int = Field(..., description="Unique identifier for the mailing.")
