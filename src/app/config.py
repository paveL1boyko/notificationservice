from os import environ as env

DATE_FORMAT = "%Y-%m-%d %H:%M:%S"
SERVER_TYPE = env.get("SERVER_TYPE", "")


class Config:
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    STATIC_FOLDER = "static"
    SECRET_KEY = env.get("SECRET_KEY")
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_ENGINE_OPTIONS = {"pool_reset_on_return": "commit", "pool_recycle": 1000}
    SQLALCHEMY_DATABASE_URI = env.get(
        "SQLALCHEMY_DATABASE_URI",
        "postgresql://postgres:postgres@0.0.0.0:5433/postgres",
    )
    # Admin
    FLASK_ADMIN_SWATCH = "slate"

    CELERY_BROKER_URL = env.get("CELERY_BROKER_URL", "redis://0.0.0.0:6378/0")
    # CELERY_IMPORTS = ("app.adapters.celery_task.task",)


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class TestingConfig(Config):
    TESTING = True
    DEBUG = True
    SQLALCHEMY_ECHO = False


celery_config = {
    "result_backend": env.get("RESULT_BACKEND", "redis://0.0.0.0:6378/0"),
    "broker_connection_retry_on_startup": True,
    "task_default_queue": f"local-{SERVER_TYPE.lower()}",
    "imports": ("app.adapters.celery_task.task",),
}
CHUNK_SIZE = 50  # Number of requests to send in one batch
BASE_URL = env.get("BASE_URL", "https://probe.fbrq.cloud/docs")
JWT_TOKEN = env.get("JWT_TOKEN")
