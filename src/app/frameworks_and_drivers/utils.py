import datetime
import typing as t

from flask.json.provider import DefaultJSONProvider


class CustomJSONProvider(DefaultJSONProvider):
    def dumps(self, obj: t.Any, **kwargs: t.Any) -> str:
        def handle_datetime(o):
            if isinstance(o, datetime.datetime):
                return o.isoformat()
            raise TypeError(f"Object of type {type(o).__name__} is not JSON serializable")

        kwargs.setdefault("default", handle_datetime)
        return super().dumps(obj, **kwargs)
