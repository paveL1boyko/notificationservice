from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, Enum, BigInteger
from sqlalchemy import Table, ForeignKey
from sqlalchemy.dialects.postgresql import ARRAY
from sqlalchemy.orm import relationship

from app.adapters.sql_alcemy import db

client_tags_table = Table(
    "client_tags",
    db.metadata,
    Column("client_id", Integer, ForeignKey("clients.id"), primary_key=True),
    Column("tag_id", Integer, ForeignKey("tags.id"), primary_key=True),
)


class Tag(db.Model):
    __tablename__ = "tags"

    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True)

    def __str__(self):
        return f"{self.__class__.__name__}: {self.name}"


class Client(db.Model):
    __tablename__ = "clients"

    id = Column(Integer, primary_key=True)
    phone_number = Column(BigInteger, unique=True)
    operator_code = Column(Integer)
    timezone = Column(String(60))

    tags = relationship("Tag", secondary=client_tags_table, backref="clients")
    messages = relationship("Message", back_populates="client")

    def __str__(self):
        return f"{self.__class__.__name__}: {self.id}, with {self.phone_number}"


class Mailing(db.Model):
    __tablename__ = "mailings"

    id = Column(Integer, primary_key=True)
    start_time = Column(DateTime, default=datetime.utcnow)
    message_text = Column(String)
    phone_numbers_filter = Column(ARRAY(BigInteger))  # List of phone numbers
    tags_filter = Column(ARRAY(String))  # List of tags
    end_time = Column(DateTime)

    messages = relationship("Message", back_populates="mailing")

    def __str__(self):
        return f"{self.__class__.__name__}: {self.id} with {self.message_text}"


class Message(db.Model):
    __tablename__ = "messages"

    id = Column(Integer, primary_key=True)
    creation_time = Column(DateTime, default=datetime.utcnow)
    status = Column(Enum("sent", "failed", name="status_enum"))
    mailing_id = Column(Integer, ForeignKey("mailings.id"))
    client_id = Column(Integer, ForeignKey("clients.id"))

    mailing = relationship("Mailing", back_populates="messages")
    client = relationship("Client", back_populates="messages")

    def __str__(self):
        return f"{self.__class__.__name__}: {self.id}, with {self.status}"
