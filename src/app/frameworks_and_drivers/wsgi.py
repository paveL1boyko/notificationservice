from flask import Flask, Blueprint

from app import config
from app.adapters.admin_panel import admin
from app.adapters.admin_panel.urls import IndexView
from app.adapters.api.client.flask_client_controller import client_bp
from app.adapters.api.client.flask_mailing_controller import mailing_bp
from app.adapters.api_doc import api_doc
from app.adapters.celery_task import celery
from app.adapters.sql_alcemy import db
from app.config import celery_config
from app.frameworks_and_drivers.utils import CustomJSONProvider

# added CustomJSONProvider fix serializing date
Flask.json_provider_class = CustomJSONProvider

app = Flask(__name__)

if config.SERVER_TYPE.lower() in ("local", "testing"):
    app.config.from_object(config.TestingConfig)
elif config.SERVER_TYPE.lower() == "dev":
    app.config.from_object(config.DevelopmentConfig)

# api blueprints
api_blueprint = Blueprint("api", __name__, url_prefix="/api")
api_blueprint.register_blueprint(client_bp)
api_blueprint.register_blueprint(mailing_bp)

app.register_blueprint(api_blueprint)

api_doc.register(app)

db.init_app(app)

# init celery_task
celery.conf.update(app.config)
celery.conf.update(celery_config)


admin.init_app(app, index_view=IndexView(name="Notification service"))
