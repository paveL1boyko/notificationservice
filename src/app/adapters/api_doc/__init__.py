from spectree import SpecTree

api_doc = SpecTree(
    path="docks",
    backend_name="flask",
    title="Notification servise API",
    mode="strict",
)
