from http import HTTPStatus

from flask import Blueprint, request, jsonify
from flask.views import MethodView
from spectree import Response

from app.adapters.api_doc import api_doc
from app.adapters.celery_task.task import send_mailing_to_client
from app.adapters.sql_alcemy.mailing_repository import SQLAlchemyMailingRepository
from app.entities.errors.mailing_errors import (
    MailingDatabaseError,
    MailingNotFoundError,
)
from app.entities.mailing.mailing_entity import (
    MailingEntityResponse,
    MailingEntityRequest,
)
from app.use_cases.mailing_use_case import MailingUseCase

MAIN_API_DOC_TAG = "Mailing"

mailing_bp = Blueprint("mailing", __name__, url_prefix="mailing")


@mailing_bp.errorhandler(MailingNotFoundError)
def handle_mailing_not_found_error(error):
    """
    Handle errors when a mailing is not found in the database.

    :param error: Exception instance
    :return: JSON response with error details
    """
    response = jsonify({"error": str(error)})
    response.status_code = HTTPStatus.NOT_FOUND.value
    return response


@mailing_bp.errorhandler(MailingDatabaseError)
def handle_mailing_database_error(error):
    """
    Handle generic database errors related to mailings.

    :param error: Exception instance
    :return: JSON response with error details
    """
    response = jsonify({"error": "An internal error occurred. Please try again later."})
    response.status_code = HTTPStatus.INTERNAL_SERVER_ERROR.value
    return response


class MailingView(MethodView):
    """
    API endpoints for mailing management.
    """

    def __init__(self):
        self.use_case = MailingUseCase(SQLAlchemyMailingRepository())

    @api_doc.validate(
        json=MailingEntityRequest,
        resp=Response(
            HTTP_201=MailingEntityResponse,
        ),
        tags=[MAIN_API_DOC_TAG],
    )
    def post(self):
        """
        Create a new mailing.

        Receives mailing data in JSON format, validates it, and adds the mailing to the database.
        Returns the ID of the newly created mailing and a success message.
        """
        mailing_data = MailingEntityRequest(**request.json)
        mailing = self.use_case.add(mailing_data)
        send_mailing_to_client.apply_async(
            args=[mailing.id],
            eta=mailing.start_time,
        )
        return MailingEntityResponse(**mailing.dict()).dict(), 201

    @api_doc.validate(
        json=MailingEntityRequest,
        resp=Response(
            f"HTTP_{HTTPStatus.NOT_FOUND}",
            HTTP_200=MailingEntityResponse,
        ),
        tags=[MAIN_API_DOC_TAG],
    )
    def put(self, mailing_id: int):
        """
        Update an existing mailing.

        Receives the mailing ID to be updated and the new mailing data in JSON format.
        Validates the data and updates the mailing in the database.
        Returns the ID of the updated mailing and a success message.
        """
        mailing_data = MailingEntityRequest(**request.json)
        mailing = self.use_case.update(mailing_id, mailing_data)
        return MailingEntityResponse(**mailing.dict()).dict(), 200

    @api_doc.validate(
        resp=Response(
            f"HTTP_{HTTPStatus.NOT_FOUND}",
            HTTP_204=None,
        ),
        tags=[MAIN_API_DOC_TAG],
    )
    def delete(self, mailing_id: int):
        """
        Delete an existing mailing.

        Receives the mailing ID to be deleted and removes the mailing from the database.
        Returns the ID of the deleted mailing and a success message.
        """
        self.use_case.delete(mailing_id)
        return "", 204


mailing_view = MailingView.as_view("mailing_view")

mailing_bp.add_url_rule("/", view_func=mailing_view, methods=["POST"])
mailing_bp.add_url_rule("/<int:mailing_id>", view_func=mailing_view, methods=["PUT", "DELETE"])
