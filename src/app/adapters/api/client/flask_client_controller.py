from flask import Blueprint, request
from flask.views import MethodView
from spectree import Response

from app.adapters.api_doc import api_doc
from app.adapters.sql_alcemy.client_repository import SQLAlchemyClientRepository
from app.entities.client.client_entity import ClientEntityResponse, ClientEntityRequest
from app.use_cases.client_use_case import ClientUseCase

MAIN_API_DOC_TAG = "Client"

client_bp = Blueprint("client", __name__, url_prefix="client")


class ClientView(MethodView):
    """
    API endpoints for client management.
    """

    def __init__(self):
        self.use_case = ClientUseCase(SQLAlchemyClientRepository())

    @api_doc.validate(
        json=ClientEntityRequest,
        resp=Response(HTTP_201=ClientEntityResponse),
        tags=[MAIN_API_DOC_TAG],
    )
    def post(self):
        """
        Create a new client.

        Receives client data in JSON format, validates it, and adds the client to the database.
        Returns the ID of the newly created client and a success message.
        """
        client_data = ClientEntityRequest(**request.json)
        client = self.use_case.create(client_data)
        return ClientEntityResponse(**client.dict()).dict(), 201

    @api_doc.validate(
        json=ClientEntityRequest,
        resp=Response(HTTP_201=None),
        tags=[MAIN_API_DOC_TAG],
    )
    def put(self, client_id: int):
        """
        Update an existing client.

        Receives the client ID to be updated and the new client data in JSON format.
        Validates the data and updates the client in the database.
        Returns the ID of the updated client and a success message.
        """
        client_data = ClientEntityRequest(**request.json)
        client = self.use_case.update(client_id, client_data)
        return ClientEntityResponse(**client.dict()).dict(), 200

    @api_doc.validate(
        resp=Response(HTTP_201=None),
        tags=[MAIN_API_DOC_TAG],
    )
    def delete(self, client_id: int):
        """
        Delete an existing client.

        Receives the client ID to be deleted and removes the client from the database.
        Returns the ID of the deleted client and a success message.
        """
        self.use_case.delete(client_id)
        return "", 204


client_view = ClientView.as_view("client_view")

client_bp.add_url_rule("/", view_func=client_view, methods=["POST"])
client_bp.add_url_rule("/<int:client_id>", view_func=client_view, methods=["PUT", "DELETE"])
