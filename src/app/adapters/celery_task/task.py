import asyncio
import logging

import aiohttp
from celery import Task

from app.adapters.celery_task import celery
from app.adapters.mailing_service.models import RequestModel
from app.adapters.mailing_service.service import chunks, process_chunk
from app.config import CHUNK_SIZE

logger = logging.getLogger(__name__)


class BaseTask(Task):
    ...


@celery.task(base=BaseTask)
def send_mailing_to_client(mailing_id, mailing_repo=None, client_repo=None):
    """
    Send asynchronous requests in chunks for a given mailing ID.

    :param mailing_id: ID of the mailing to process.
    :param mailing_repo: Mailing repository instance. Default is None.
    :param client_repo: Client repository instance. Default is None.
    """
    from app.frameworks_and_drivers.wsgi import app

    with app.app_context():
        if mailing_repo is None:
            from app.use_cases.mailing_use_case import MailingUseCase
            from app.adapters.sql_alcemy.mailing_repository import SQLAlchemyMailingRepository

            mailing_repo = MailingUseCase(SQLAlchemyMailingRepository())

        if client_repo is None:
            from app.use_cases.client_use_case import ClientUseCase
            from app.adapters.sql_alcemy.client_repository import SQLAlchemyClientRepository

            client_repo = ClientUseCase(SQLAlchemyClientRepository())

        mailing = mailing_repo.get_mailing_by_id(mailing_id=mailing_id)
        clients_for_mailing = client_repo.get_clients_by_tags_or_phone(
            tags=mailing.tags_filter, phones=mailing.phone_numbers_filter
        )
        deadline = (mailing.end_time - mailing.start_time).total_seconds()

        async def main():
            async with asyncio.timeout(deadline):
                async with aiohttp.ClientSession() as session:
                    all_tasks = [
                        (
                            RequestModel(
                                id=client.id, phone=client.phone_number, text=mailing.message_text
                            ),
                            client.id,
                        )
                        for client in clients_for_mailing
                    ]
                    async for chunk in chunks(all_tasks, CHUNK_SIZE):
                        await process_chunk(session, chunk, mailing_id)

        asyncio.run(main())
