from celery import Celery
from celery.app.task import Task

from app import config

Task.__class_getitem__ = classmethod(lambda cls, *args, **kwargs: cls)  # type: ignore[attr-defined]


celery = Celery(__name__, broker=config.Config.CELERY_BROKER_URL)
