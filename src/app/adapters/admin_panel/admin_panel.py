from flask_admin.contrib.sqla import ModelView
from flask_admin.contrib.sqla.ajax import QueryAjaxModelLoader

from app.adapters.sql_alcemy.base_repository import BaseSQLAlchemyRepository
from app.frameworks_and_drivers.orm_models.models import Tag


class ClientAdminView(ModelView):
    column_list = (
        "phone_number",
        "operator_code",
        "timezone",
        "tags",
    )

    form_ajax_refs = {
        "tags": QueryAjaxModelLoader(
            "tags",
            BaseSQLAlchemyRepository.db.session,
            Tag,
            fields=[Tag.name],
            page_size=10,
            minimum_input_length=0,
        ),
    }


class TagAdminView(ModelView):
    ...


class MailingAdminView(ModelView):
    column_list = (
        "start_time",
        "message_text",
        "phone_numbers_filter",
        "tags_filter",
        "end_time",
    )
