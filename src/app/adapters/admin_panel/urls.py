from flask_admin import AdminIndexView, expose

from app.adapters.admin_panel import admin
from app.adapters.admin_panel.admin_panel import (
    ClientAdminView,
    TagAdminView,
    MailingAdminView,
)
from app.adapters.sql_alcemy.base_repository import BaseSQLAlchemyRepository
from app.frameworks_and_drivers.orm_models.models import Client, Tag, Mailing


class IndexView(AdminIndexView):
    def is_visible(self):
        return False

    @expose("/")
    def index(self):
        return self.render("admin/index.html")


admin.add_view(ClientAdminView(Client, BaseSQLAlchemyRepository.db.session, category="Client"))
admin.add_view(TagAdminView(Tag, BaseSQLAlchemyRepository.db.session, category="Client"))
admin.add_view(MailingAdminView(Mailing, BaseSQLAlchemyRepository.db.session, category="Mailing"))
