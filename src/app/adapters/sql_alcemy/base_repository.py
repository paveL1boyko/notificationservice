import logging

from app.adapters.sql_alcemy import db
from app.interfaces.base_repository import BaseRepositoryInterface

logger = logging.getLogger(__name__)


class BaseSQLAlchemyRepository(BaseRepositoryInterface):
    db = db

    def drop_database(self):
        self.db.session.close()
        self.db.drop_all()
        logger.info("Database dropped successfully")

    def create_database(self):
        self.db.create_all()
        logger.info("Database created successfully")
