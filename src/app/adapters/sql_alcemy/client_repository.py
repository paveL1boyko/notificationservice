import logging

from sqlalchemy import or_
from sqlalchemy.exc import IntegrityError, SQLAlchemyError

from app.entities.client.client_dto import ClientDTO
from app.entities.errors.client_errors import ClientNotFoundError, ClientAlreadyExistsError
from app.frameworks_and_drivers.orm_models.models import Client, Tag
from app.interfaces.client_repository import ClientRepositoryInterface
from .base_repository import BaseSQLAlchemyRepository

logger = logging.getLogger(__name__)


class SQLAlchemyClientRepository(BaseSQLAlchemyRepository, ClientRepositoryInterface):
    def _commit_changes(self, client: Client):
        """
        Commit changes to the database and handle potential errors.
        """
        try:
            self.db.session.commit()
        except IntegrityError as err:
            self.db.session.rollback()
            raise ClientAlreadyExistsError(str(client)) from err
        except SQLAlchemyError as e:
            self.db.session.rollback()
            logger.error(f"SQLAlchemy error occurred: {str(e)}")
            raise

    def create(self, client: ClientDTO) -> ClientDTO:
        """
        Add a new client to the database.

        :param client: Client data transfer object
        :return: ClientDTO of the added client
        """
        db_client = Client(**client.dict(exclude={"tags"}))

        # Handling tags
        tags_for_client = []
        for tag_name in client.tags:
            # Check if the tag exists in the database
            tag = self.db.session.query(Tag).filter_by(name=tag_name).first()
            if not tag:
                # If the tag doesn't exist, create a new one
                tag = Tag(name=tag_name)
                self.db.session.add(tag)
                logger.info(f"New tag '{tag_name}' created.")
            else:
                logger.info(f"Tag '{tag_name}' already exists. Linking to the client.")
            tags_for_client.append(tag)

        db_client.tags = tags_for_client
        self.db.session.add(db_client)
        self._commit_changes(db_client)
        logger.info(f"Client {client} added successfully.")
        return ClientDTO.from_orm(db_client)

    def delete(self, client_id: int) -> bool:
        """
        Delete a client from the database by its ID.

        :param client_id: ID of the client to delete
        :return: True if deletion was successful, False otherwise
        """
        db_client = self.db.session.get(Client, client_id)
        if not db_client:
            logger.warning(f"Client with ID {client_id} not found for deletion")
            return False

        self.db.session.delete(db_client)
        self._commit_changes(db_client)
        logger.info(f"Client with ID {client_id} deleted successfully")
        return True

    def update(self, client_id: int, client: ClientDTO) -> ClientDTO:
        """
        Update a client's details in the database.

        :param client_id: ID of the client to update
        :param client: Client data transfer object with updated details
        :return: ClientDTO of the updated client
        """
        db_client = self.db.session.get(Client, client_id)
        if not db_client:
            raise ClientNotFoundError(str(db_client))

        for key, value in client.dict(exclude={"id", "tags"}).items():
            setattr(db_client, key, value)
        db_client.tags = [Tag(name=tag) for tag in client.tags]
        self._commit_changes(db_client)
        logger.info(f"Client with ID {client_id} updated successfully")
        return ClientDTO(id=client_id, **client.dict(exclude={"id"}))

    def get_client_by_id(self, client_id: int) -> ClientDTO | None:
        """
        Retrieve a client by its ID.

        :param client_id: ID of the client to retrieve
        :return: ClientDTO of the retrieved client or None if not found
        """
        db_client = self.db.session.get(Client, client_id)
        if not db_client:
            logger.error(f"No client found with ID: {client_id}")
            return None
        return ClientDTO.from_orm(db_client)

    def get_clients_by_tags_or_phone(
        self, tags: list[str] | None = None, phones: list[int] | None = None
    ) -> list[ClientDTO]:
        """
        Retrieve clients based on a list of tags and/or a phone number.

        :param tags: List of tags to filter clients.
        :param phones: Optional phone number to further filter clients.
        :return: List of ClientDTO objects matching the criteria.
        """
        query = self.db.session.query(Client)

        filters = []

        if tags:
            filters.append(Client.tags.any(Tag.name.in_(tags)))

        if phones:
            filters.append(Client.phone_number.in_(phones))

        if filters:
            query = query.filter(or_(*filters))

        db_clients = query.all()

        return [ClientDTO.from_orm(client) for client in db_clients]
