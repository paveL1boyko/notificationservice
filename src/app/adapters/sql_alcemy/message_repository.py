import logging

from sqlalchemy.exc import SQLAlchemyError

from app.adapters.sql_alcemy.base_repository import BaseSQLAlchemyRepository
from app.entities.errors.message_errors import (
    MessageDatabaseError,
    MessageNotFoundError,
)
from app.entities.message.message_dto import MessageDTO
from app.frameworks_and_drivers.orm_models.models import Message
from app.interfaces.message_repository import MessageRepositoryInterface

logger = logging.getLogger(__name__)


class SQLAlchemyMessageRepository(BaseSQLAlchemyRepository, MessageRepositoryInterface):
    def _commit_changes(self):
        """
        Commit changes to the database and handle potential errors.
        """
        try:
            self.db.session.commit()
        except SQLAlchemyError as e:
            self.db.session.rollback()
            logger.error(f"SQLAlchemy error occurred: {str(e)}")
            raise MessageDatabaseError from e

    def create(self, message: MessageDTO) -> MessageDTO:
        """
        Add a new message to the database.

        :param message: Message data transfer object
        :return: MessageDTO of the added message
        """
        db_message = Message(**message.dict())
        self.db.session.add(db_message)
        self._commit_changes()
        logger.info(f"Message {message} added successfully")
        return MessageDTO.from_orm(db_message)

    def delete(self, message_id: int) -> bool:
        """
        Delete a message from the database by its ID.

        :param message_id: ID of the message to delete
        :return: True if deletion was successful, False otherwise
        """
        db_message = self.db.session.get(Message, message_id)
        if not db_message:
            logger.warning(f"Message with ID {message_id} not found for deletion")
            raise MessageNotFoundError(str(db_message))

        self.db.session.delete(db_message)
        self._commit_changes()
        logger.info(f"Message with ID {message_id} deleted successfully")
        return True

    def update(self, message_id: int, message: MessageDTO) -> MessageDTO:
        """
        Update a message's details in the database.

        :param message_id: ID of the message to update
        :param message: Message data transfer object with updated details
        :return: MessageDTO of the updated message
        """
        db_message = self.db.session.get(Message, message_id)
        if not db_message:
            logger.warning(f"Message with ID {message_id} not found for update")
            raise MessageNotFoundError(str(db_message))

        for key, value in message.dict(exclude={"id"}).items():
            setattr(db_message, key, value)
        self._commit_changes()
        logger.info(f"Message with ID {message_id} updated successfully")
        return MessageDTO(id=message_id, **message.dict(exclude={"id"}))

    def get_message_by_id(self, message_id: int) -> MessageDTO | None:
        """
        Retrieve a message by its ID.

        :param message_id: ID of the message to retrieve
        :return: MessageDTO of the retrieved message or None if not found
        """
        db_message = self.db.session.get(Message, message_id)
        if not db_message:
            logger.error(f"No message found with ID: {message_id}")
            return None
        return MessageDTO.from_orm(db_message)

    def get_message_by_mailing_id(self, mailing_id: int) -> list[MessageDTO]:
        """
        Retrieve all messages by their mailing ID.

        :param mailing_id: Mailing ID of the messages to retrieve
        :return: List of MessageDTO of the retrieved messages or an empty list if none found
        """
        db_messages = Message.query.filter_by(mailing_id=mailing_id).all()
        if not db_messages:
            logger.warning(f"No messages found with mailing ID: {mailing_id}")
            return []

        return [MessageDTO.from_orm(db_message) for db_message in db_messages]

    def create_or_update(self, message: MessageDTO) -> MessageDTO:
        """
        Add a new message to the database or update an existing one.

        :param message: Message data transfer object
        :return: MessageDTO of the added or updated message
        """
        db_message = (
            self.db.session.query(Message)
            .filter_by(client_id=message.client_id, mailing_id=message.mailing_id)
            .one_or_none()
        )
        if db_message:
            db_message.status = message.status
            logger.info(f"Message {message} updated successfully")
        else:
            db_message = Message(**message.dict())
            self.db.session.add(db_message)
            logger.info(f"Message {message} added successfully")

        self._commit_changes()

        return MessageDTO.from_orm(db_message)
