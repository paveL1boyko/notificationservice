import logging

from sqlalchemy.exc import SQLAlchemyError

from app.adapters.sql_alcemy.base_repository import BaseSQLAlchemyRepository
from app.entities.errors.mailing_errors import (
    MailingDatabaseError,
    MailingNotFoundError,
)
from app.entities.mailing.mailing_dto import MailingDTO
from app.frameworks_and_drivers.orm_models.models import Mailing
from app.interfaces.mailing_repository import MailingRepositoryInterface

logger = logging.getLogger(__name__)


class SQLAlchemyMailingRepository(BaseSQLAlchemyRepository, MailingRepositoryInterface):
    def _commit_changes(self):
        """
        Commit changes to the database and handle potential errors.
        """
        try:
            self.db.session.commit()
        except SQLAlchemyError as e:
            self.db.session.rollback()
            logger.error(f"SQLAlchemy error occurred: {str(e)}")
            raise MailingDatabaseError from e

    def create(self, mailing: MailingDTO) -> MailingDTO:
        """
        Add a new mailing to the database.

        :param mailing: Mailing data transfer object
        :return: MailingDTO of the added mailing
        """
        db_mailing = Mailing(**mailing.dict())
        self.db.session.add(db_mailing)
        self._commit_changes()
        logger.info(f"Mailing {mailing} added successfully")
        return MailingDTO.from_orm(db_mailing)

    def delete(self, mailing_id: int) -> bool:
        """
        Delete a mailing from the database by its ID.

        :param mailing_id: ID of the mailing to delete
        :return: True if deletion was successful, False otherwise
        """
        db_mailing = self.db.session.get(Mailing, mailing_id)
        if not db_mailing:
            logger.warning(f"Mailing with ID {mailing_id} not found for deletion")
            raise MailingNotFoundError(f"Mailing with ID {mailing_id} not found")

        self.db.session.delete(db_mailing)
        self._commit_changes()
        logger.info(f"Mailing with ID {mailing_id} deleted successfully")
        return True

    def update(self, mailing_id: int, mailing: MailingDTO) -> MailingDTO:
        """
        Update a mailing's details in the database.

        :param mailing_id: ID of the mailing to update
        :param mailing: Mailing data transfer object with updated details
        :return: MailingDTO of the updated mailing
        """
        db_mailing = self.db.session.get(Mailing, mailing_id)
        if not db_mailing:
            logger.warning(f"Mailing with ID {mailing_id} not found for update")
            raise MailingNotFoundError(f"Mailing with ID {mailing_id} not found")

        for key, value in mailing.dict(exclude={"id"}).items():
            setattr(db_mailing, key, value)
        self._commit_changes()
        logger.info(f"Mailing with ID {mailing_id} updated successfully")
        return MailingDTO(id=mailing_id, **mailing.dict(exclude={"id"}))

    def get_mailing_by_id(self, mailing_id: int) -> MailingDTO | None:
        """
        Retrieve a mailing by its ID.

        :param mailing_id: ID of the mailing to retrieve
        :return: MailingDTO of the retrieved mailing or None if not found
        """
        db_mailing = self.db.session.get(Mailing, mailing_id)
        if not db_mailing:
            logger.error(f"No mailing found with ID: {mailing_id}")
            return None
        return MailingDTO.from_orm(db_mailing)
