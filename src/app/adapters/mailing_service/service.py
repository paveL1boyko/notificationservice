import asyncio
import logging
from random import random

import aiohttp
from tenacity import stop_after_attempt, wait_fixed, AsyncRetrying

from app.adapters.mailing_service.models import RequestModel, ResponseModel
from app.adapters.sql_alcemy.message_repository import SQLAlchemyMessageRepository
from app.config import JWT_TOKEN, BASE_URL
from app.entities.message.message_dto import StatusEnum
from app.entities.message.message_entity import MessageEntityRequest
from app.use_cases.message_use_case import MessageUseCase

message_use_case = MessageUseCase(SQLAlchemyMessageRepository())


logger = logging.getLogger(__name__)

RETRIES = AsyncRetrying(
    wait=wait_fixed(5),
    stop=stop_after_attempt(5),
    after=lambda retry_state: logger.warning(
        f"Request failed. Retrying... Error: {retry_state.outcome.exception()}"
    ),
    reraise=True,
)


async def fetch(
    session: aiohttp.ClientSession,
    data: RequestModel,
    client_id: int,
    mailing_id: int,
    url: str = BASE_URL,
) -> ResponseModel | None:
    """
    Asynchronously send a POST request with the provided data.

    :param session: The aiohttp session.
    :param data: Data for the POST request.
    :param client_id: Client's ID.
    :param mailing_id: Mailing's ID.
    :param url: URL to send the POST request to. Default is BASE_URL.
    :return: ResponseModel if successful, None otherwise.
    """
    headers = {"Authorization": f"Bearer {JWT_TOKEN}"}
    message_data = MessageEntityRequest(mailing_id=mailing_id, client_id=client_id)

    async for attempt in RETRIES:
        with attempt:
            async with session.post(url, json=data.dict(), headers=headers) as response:
                try:
                    response.raise_for_status()
                    resp_data = await response.json()
                    message_use_case.create_or_update(message_data)
                    return ResponseModel(**resp_data)
                except aiohttp.ClientError:
                    message_data.status = StatusEnum.failed
                    message_use_case.create_or_update(message_data)
                    raise


async def process_chunk(
    session: aiohttp.ClientSession, chunk: list[tuple[RequestModel, int]], mailing_id: int
):
    """
    Process a chunk of tasks by sending them asynchronously.

    :param session: The aiohttp session.
    :param chunk: A list of tasks to process.
    :param mailing_id: Mailing's ID.
    """
    await asyncio.gather(
        *[fetch(session, data, client_id, mailing_id) for data, client_id in chunk]
    )


async def chunks(tasks: list[tuple[RequestModel, int]], chunk_size: int):
    """
    Yield successive n-sized chunks from tasks.

    :param tasks: The list to split.
    :param chunk_size: The chunk size.
    :yield: Chunks of the original list.
    """
    for i in range(0, len(tasks), chunk_size):
        yield tasks[i : i + chunk_size]
        await asyncio.sleep(random())  # nosec
