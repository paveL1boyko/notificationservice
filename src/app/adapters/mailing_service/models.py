from pydantic.v1 import BaseModel


class RequestModel(BaseModel):
    """Model for the request payload."""

    id: int
    phone: int
    text: str


class ResponseModel(BaseModel):
    """Model for the response payload."""

    code: int
    message: str
