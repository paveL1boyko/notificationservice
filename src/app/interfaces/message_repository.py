from abc import ABC, abstractmethod

from app.entities.message.message_dto import MessageDTO


class MessageRepositoryInterface(ABC):
    @abstractmethod
    def create(self, message: MessageDTO) -> MessageDTO:
        """
        Add a new message to the repository.

        :param message: Data for the new message.
        :return: The created message with its ID.
        """
        ...

    @abstractmethod
    def delete(self, message_id: int) -> bool:
        """
        Delete a message from the repository.

        :param message_id: ID of the message to delete.
        :return: True if the deletion was successful, False otherwise.
        """
        ...

    @abstractmethod
    def update(self, message_id: int, message: MessageDTO) -> MessageDTO:
        """
        Update an existing message in the repository.

        :param message_id: ID of the message to update.
        :param message: New data for the message.
        :return: The updated message.
        """
        ...

    @abstractmethod
    def get_message_by_id(self, message_id: int) -> MessageDTO:
        """
        Retrieve a message by its ID.

        :param message_id: ID of the message to retrieve.
        :return: The message data.
        """
        ...

    @abstractmethod
    def get_message_by_mailing_id(self, mailing_id: int) -> list[MessageDTO]:
        """
        Retrieve a message by its ID.

        :param mailing_id: ID of the mailing  to retrieve.
        :return: The message data.
        """
        ...

    @abstractmethod
    def create_or_update(self, message: MessageDTO) -> MessageDTO:
        """
        Add a new message to the database or update an existing one.

        :param message: Message data transfer object
        :return: MessageDTO of the added or updated message
        """
        ...
