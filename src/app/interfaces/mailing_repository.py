from abc import ABC, abstractmethod

from app.entities.mailing.mailing_dto import MailingDTO


class MailingRepositoryInterface(ABC):
    @abstractmethod
    def create(self, mailing: MailingDTO) -> MailingDTO:
        ...

    @abstractmethod
    def delete(self, mailing_id: int) -> bool:
        ...

    @abstractmethod
    def update(self, mailing_id: int, mailing: MailingDTO) -> MailingDTO:
        ...

    @abstractmethod
    def get_mailing_by_id(self, mailing_id: int):
        ...
