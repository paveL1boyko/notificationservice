from abc import ABC, abstractmethod


class BaseRepositoryInterface(ABC):
    @abstractmethod
    def drop_database(self):
        ...

    @abstractmethod
    def create_database(self):
        ...
