from abc import ABC, abstractmethod

from app.entities.client.client_dto import ClientDTO


class ClientRepositoryInterface(ABC):
    @abstractmethod
    def create(self, client: ClientDTO) -> ClientDTO:
        ...

    @abstractmethod
    def delete(self, client_id: int) -> bool:
        ...

    @abstractmethod
    def update(self, client_id: int, client: ClientDTO) -> ClientDTO:
        ...

    @abstractmethod
    def get_client_by_id(self, client_id: int):
        ...

    @abstractmethod
    def get_clients_by_tags_or_phone(
        self, tags: list[str] | None = None, phones: list[int] | None = None
    ) -> list[ClientDTO]:
        """
        Retrieve clients based on a list of tags and optionally a phone number.

        :param tags: List of tags to filter clients.
        :param phones: List of phone number to further filter clients.
        :return: List of ClientDTO objects matching the criteria.
        """
        ...
