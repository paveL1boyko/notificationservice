# *NotificationService*

## **Key Features**

- **Code Testing**: The project includes unit tests and integration tests to ensure code correctness.

- **GitLab CI**: Automated build and testing of the code is set up for every push to the repository.

- **Docker-compose**: Included is a `docker-compose.yml` file that allows all project services to be started with a
  single command.

- **Swagger UI**: Integrated Swagger for automatic API documentation generation and Redoc.

- **Web UI**: A web interface has been developed for managing mailings and viewing statistics.

- **Error Handling**: Implemented a retry mechanism for message sending in case of errors.

- **Logging**: Detailed logging of all operations within the service.

## **Installation and Running**

1. **Clone the repository**:

```
git clone https://gitlab.com/paveL1boyko/notificationservice.git
```

## **Navigate to the project directory**:

```
cd [project directory name]
```

## **Install the necessary dependencies**:

## Prerequisites

:warning: **Make sure you have `poetry` installed on your system.** If you don't have it, please install it before
proceeding. Depending on your OS, you can use the following commands:

- **For macOS**:
  ```bash
  brew install poetry
  ```
- **For Debian/Ubuntu:**:
  ```bash
  sudo apt-get install poetry
  ```
- **For Red Hat/Fedora:**:
  ```bash
  sudo yum install poetry
  ```

```
poetry install && invoke setup
```

## **Running the Project Locally**:

***To run the project in a local development environment:***

:warning: **Before executing this command,
you need to create a `.env` file similar to `.env.local`
for project setup. After launching the project using the
`invoke local-dev` command, you can run the project through
the debugger using the `wsgi.py` file. The environment
variables from the `.env.local` file are used for this purpose.**

```
invoke local-dev
```

## **API Documentation**

To view the API documentation, navigate to `/docs/swagger`
or `/docks/redoc/` in your browser.

# Contributing

If you wish to contribute to the project's development, please refer to the [Contribution Guide](./CONTRIBUTING.md).

# License

This project is licensed under [MIT]. For more details, see the [LICENSE.md](./LICENSE.md) file.
