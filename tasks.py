import os

from invoke import task

DOCKER_TAG = "notification_service"


def load_env_vars_from_file(filename):
    with open(filename) as f:
        for line in f:
            cleaned_line = line.split("#")[0].strip()
            if cleaned_line:
                name, value = cleaned_line.split("=", 1)
                os.environ[name] = value


load_env_vars_from_file("./src/.env.local")


@task
def setup(ctx):
    ctx.run("pre-commit install")


@task
def build_images(ctx):
    ctx.run(f"docker build -t {DOCKER_TAG} .")


@task(pre=[build_images])
def local_dev(ctx):
    ctx.run(
        "docker compose -f docker-compose-base.yml "
        "-f docker-compose-local.yml up -d --remove-orphans"
    )


@task(pre=[build_images])
def test_full(ctx):
    ctx.run("docker-compose -f docker-compose-test.yml up --abort-on-container-exit")


@task
def start_celery(ctx):
    with ctx.cd("src"):
        ctx.run(
            "celery -A app.frameworks_and_drivers.wsgi.celery worker"
            " --loglevel=info -B -E --concurrency=2"
        )
