# Contributing to [NotificationService]

First off, thank you for considering contributing to [NotificationService]. It's people like you that
make [NotificationService] such a great tool.

## Where to Start?

- **Beginner**: If you're new to the project and want to help, start by checking out the issues labeled
  as `good first issue`. These are typically simpler tasks that are well-suited for first-time contributors.

- **Bug Reports**: If you think you've found a bug, first read the [Issues](#issues) section below to see if it's
  already being addressed. If not, open a new issue with a detailed description of the problem.

- **Feature Requests**: If you have a great idea for a new feature, open a new issue to discuss it. Please provide as
  much context as you can about why you think it's needed and how it should work.

## Issues

- Ensure the bug was not already reported by searching on GitLab
  under [Issues](https://gitlab.com/paveL1boyko/notificationservice/-/issues).

- If you're unable to find an open issue addressing the
  problem, [open a new one](https://gitlab.com/paveL1boyko/notificationservice/-/issues/new). Be sure to include a title
  and clear description, as much relevant information as possible, and a code sample or an executable test case
  demonstrating the expected behavior that is not occurring.

## Merge requests

1. Fork the repo and create your branch from `main`.
2. If you've added code that should be tested, add tests.
3. If you've changed APIs, update the documentation.
4. Ensure the test suite passes.
5. Make sure your code lints.
6. Issue that merge request!

## Coding Conventions

- Ensure you follow the coding conventions used throughout the project: indentation, accurate comments, etc.

- Keep your commits as atomic as possible. Each commit should represent a single logical change: don't make several
  unrelated changes in a single commit.

## License

By contributing, you agree that your contributions will be licensed under its MIT License.

## Questions?

If you have any questions or need further clarification on any of the contribution steps, feel free to ask. We
appreciate your effort and are here to help!
